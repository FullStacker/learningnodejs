// Require express, prepare app and port
const express=require("express");
const app = express();
const port = 3000;

// Creat sever that repsond hello world
app.get("/",(req,res)=>res.send("Hello World!"));

// Routing for POST handler
// Routing for PUT handler for user
// Routing for DELETE handler for user
app.post("/",(req,res)=>res.send("POST handler!"))
app.put("/user",(req,res)=>res.send("PUT handler for user folder!"))
app.delete("/user",(req,res)=>res.send("DELETE handler for user folder!"))

app.use('/static', express.static('public'))
// 404 response
app.get('*',(req, res, next)=>{res.status(404).send("Error 404: Can\'t get the target file!")})
// Generic Error handler
app.use('*',(req, res, next)=>{res.status(500).send('Error 500: Something is wrong!')})

//app.listen(port,() => console.log(`Example app listening on port ${port}!`))
// export app
module.exports=app;