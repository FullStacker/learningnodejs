const request = require('supertest');
const app = require('../app');

describe('App', function() {
  it('has the default page', function(done) {
    request(app)
      .get('/')
      .expect("Hello World!", done);
  });
  
  //Routing tests
  it('has the POST handler', function(done) {
    request(app)
      .post('/')
      .expect("POST handler!", done);
  });
  it('has the PUT handler for user', function(done) {
    request(app)
      .put('/user')
      .expect("PUT handler for user folder!", done);
  });
  it('has the DELETE handler for user', function(done) {
    request(app)
      .delete('/user')
      .expect("DELETE handler for user folder!", done);
  });

  //404 response
  it('has the 404 handler', function(done) {
    request(app)
      .get('/something doesn\'t exsit!')
      .expect("Error 404: Can't get the target file!", done)
  });

  //Error handler
  it('has the generic error handler', function(done) {
    request(app)
      .delete('/something wrong!')
      .expect("Error 500: Something is wrong!", done)
  });

}); 